/*
Name: Christopher Hurst
Class: CSC240
Due Date: 4/26/17
Files: Matrixfx.java, Matrixfx.class
Description: This program uses the javafx function in java to make a 10x10 
    random binary matrix using 1's and 0's along with buttons that change the 
    matrix to 0's, 1's and randomized, along with a quit button. 
 */
package matrixfx;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;    
import java.util.Random;
import javafx.geometry.Insets;
import javafx.scene.control.Alert;

public class MatrixFX extends Application {
   
    public static void main() {    
           
        launch();
        }     
    @Override
public void start(Stage window) {
    
    makeGreet();

    GridPane grid = new GridPane();  
    Scene scene = new Scene(grid, 400, 400);
    grid.setPadding(new Insets(10,10,10,10));

    Button zeroButton = new Button("0's");
    zeroButton.setOnAction(e -> {
        makeGrid(0, window, scene, grid, false);  
        });
    Button oneButton = new Button("1's");
    oneButton.setOnAction(e -> {
        makeGrid(1, window, scene, grid, false);     
        });
    Button randButton = new Button("Random");
    randButton.setOnAction(e -> {
        makeGrid(0, window, scene, grid, true);  
            });
        Button quitButton = new Button("Quit");
        quitButton.setOnAction(e ->{
            window.close();
            });
        grid.setConstraints(zeroButton, 0, 0);
        grid.setConstraints(oneButton, 1, 0);
        grid.setConstraints(randButton, 2, 0);
        grid.setConstraints(quitButton, 0, 12);
        grid.getChildren().addAll(zeroButton, oneButton, randButton, quitButton);
        grid.setStyle("-fx-background-color: teal;");
        window.setTitle("Random 10x10 Matrix");
        window.setScene(scene);
        window.show();
}

public static void makeGreet(){
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Random Binary Matrix (JavaFX)");
        alert.setHeaderText(null);
        alert.setContentText("Welcome to Random Binary Matrix");
        alert.showAndWait();
    }

    private static void makeGrid(int num, Stage window, Scene scene, 
        GridPane grid, boolean num2){
        
        for(int i = 1; i < 11; i++){
            for(int j = 0; j < 10; j++){  
                TextField tf = new TextField();
                tf.setPrefHeight(25);
                tf.setPrefWidth(25);
                tf.setEditable(false);
                if(num2){
                    Random rand = new Random();
                    int rand1 = rand.nextInt(2); 
                    tf.setText("" + rand1);
                }
                else{
                    tf.setText("" + num);
                }
                grid.setRowIndex(tf,i);
                grid.setColumnIndex(tf,j);    
                grid.getChildren().add(tf); 
            }
        }
        window.setScene(scene);
        window.show();
    }
}



